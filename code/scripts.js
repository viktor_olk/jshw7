// - Написати функцію `filterBy()`, яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
let sourceArr = [
  "hello",
  "world",
  23,
  "23",
  null,
  false,
  undefined,
  { a: 6 },
  NaN,
];

function filterBy(arr, type) {
  let newArr = [];
  arr.map((item) => {
    if (typeof item !== type) {
      newArr.push(item);
    }
  });
  console.log(arr);
  console.log(newArr);
}

filterBy(sourceArr, "object");
